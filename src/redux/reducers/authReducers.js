import {
  SAVE_TOKEN,
  LOGIN_ERROR,
  LOGIN_SUCCESS,
  LOGIN_REQUEST
} from '../actionTypes'

const initialState = {
  token: '',
  isLoading: false,
  isError: false,
  errorMessage: ""
}

// action {type: 'SAVE_TOKEN', payload: ''}
// action {type: 'DELETE_TOKEN', payload: ''}
// 

export default (state = initialState, {type, payload}) => {
  switch (type) {
    case SAVE_TOKEN:
      return {
        ...state,
        token: payload
      }
    case LOGIN_REQUEST:
      return {
        ...state, 
        isLoading: true
      }
    case LOGIN_SUCCESS: 
      return {
        ...state,
        isLoading: false,
        token: payload
      }
    case LOGIN_ERROR: 
      return {
        ...state,
        isLoading: false,
        isError: true,
        errorMessage: payload
      }
    default:
      return state
  }
}