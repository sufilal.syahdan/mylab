import { combineReducers } from 'redux';

// reducers
import authReducers from './authReducers' ;
// import projectReducers from './projectReducers';

export default combineReducers({ authReducers })